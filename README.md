# Hiking Trails - Dohi Recruiting Test

## Getting started

### Running the app locally
1. Clone the repository
2. `cd` into the repository
3. run `yarn install` (if missing yarn, replace with `npm`)
4. run `yarn start` (if missing yarn, replace with `npm`)

### Running the app hosted on Heroku
1. Visit this [link](https://hiking-trails-dohi-test.herokuapp.com/)

## Noticable libraries/frameworks/boilerplates

 1. Create React App </br>
 *Used to get this simple project started fast without wasting time for setup*
 2. react-google-maps </br>
 *Wrapper around google maps-api. Made communication with the api simpler*
 3. React Motion UI pack </br>
 *An easier way to use the React Motion-library when making simple and common UI-transitions*
 4. request </br>
 *Used to fetch data from the server*

## Explanation and motivation

The choice of using Create React App, react-google-maps, React Motion UI pack was simply to make the development faster. For example the boilerplate React Create App led me to have a react development environment to start with in no time.
Request, i used because i had used it before and i did not want to risk the time of learning a new library.