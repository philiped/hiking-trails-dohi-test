import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { withGoogleMap, GoogleMap, Polyline, Marker, InfoWindow } from 'react-google-maps';
import _ from 'lodash';

const style = {
  wrapper: {
    height: '100%',
    width: '100%',
  },

  map: {
    height: '100%',
    width: '100%',
  }
  
}

const options = {
  polyline: {
    strokeColor: '#5f2c82',
    strokeOpacity: 0.6
  }
}

const MapComponent = withGoogleMap(props => (
  <GoogleMap
    defaultZoom={16}
    center={props.position || { lat: 63.82591899999999, lng: 20.264212600000064 }}
    options={{ disableDefaultUI: true }}
  >

      <Polyline
        options={options.polyline}
        path={props.polyline[0]}
        onClick={_.noop}
        onMouseOver={() => props.onPathHover('path')}
        onMouseOut={() => props.onPathHover('')}
        onRightClick={_.noop}
        onDragStart={_.noop}
      />

      {props.places.map((place, index) => {
        return (
          <Marker
            key={index}
            position={place.place_position}
            onMouseOver={() => props.onMarkerHover(place)}
            onMouseOut={() => props.onMarkerHover('')}
          />
      )})}

  </GoogleMap>
));

class Map extends Component {

  handleMarkerHover(place) {
    this.props.onMarkerHover(place);
  }

  handlePathHover(path) {
    this.props.onPathHover(path);
  }

  // handleMapMounted(map) {
  //   console.log('this', this);
  //   this._map = map;
  // }

  render() {
    return (
      <MapComponent
        onMarkerHover={place => this.handleMarkerHover(place)}
        onPathHover={path => this.handlePathHover(path)}
        position={this.props.polyline[0][0]}
        places={this.props.places}
        polyline={this.props.polyline}
        containerElement={
          <div style={style.wrapper} />
        }
        mapElement={
          <div style={style.map} />
        }
      />
    );
  }
}

export default Map;