import React, { Component } from 'react';
import Transition from 'react-motion-ui-pack'
import rp from 'request-promise';
import Header from './Header';
import Map from './Map';
import PathList from './PathList';
import PathInfo from './PathInfo';
import PlaceInfo from './PlaceInfo';
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      selectedPath: '',
      placeHovered: '',
      pathHovered: '',
      paths: [],
      lat: 63.82591899999999,
      lng: 20.264212600000064,
    }
  }

  componentDidMount() {
    const requestOptions = { json: true };
    rp(...requestOptions, { uri: `https://forward-byte-711.appspot.com/read/Test`})
    .then(response => {
      console.log('Received paths');
      const parsedResponse = JSON.parse(response);
      this.setState({ paths: parsedResponse.paths });
    })
    .catch(err => {
      console.log('Received error');
      console.log(err);
    });
  }

  handlePathSelect(selectedPath) {
    this.setState({ selectedPath }, () => {
      console.log('selected', this.state.selectedPath.path_name.sv);
      // console.log('this.refs', this.refs);
    });
  }

  handleMarkerHover(place) {
    this.setState({ placeHovered: place}, () => {
      console.log('Marker hovered: ', this.state.placeHovered || 'none');
    });
    // this.props.onMarkerHover(place);
  }

  handlePathHover(path) {
    this.setState({ pathHovered: path}, () => {
      console.log('Path hovered: ', this.state.pathHovered || 'none');
    });
  }
  
  render() {
    return (
      <div className="App">
        <Map onMarkerHover={place => this.handleMarkerHover(place)} onPathHover={path => this.handlePathHover(path)} defaultLat={this.state.lat} defaultLng={this.state.lng} places={this.state.selectedPath.places || []} polyline={this.state.selectedPath.path_polyline || [[]]}/>
        <div style={{ position: 'absolute', top: 0, width: 200, height: '100%'}}>
          <PathList paths={this.state.paths} onPathSelect={path => this.handlePathSelect(path)} />
        </div>
        <Transition
          component={false}
          enter={{
            opacity: 1,
          }}
          leave={{
            opacity: 0,
          }}
        >
          {this.state.placeHovered &&
            <div key={'place_info'} style={{ backgroundColor: '#FFF', position: 'absolute', bottom: 0, width: '100%', height: 400 }}>
              <PlaceInfo place={this.state.placeHovered} />
            </div>
          }
          {this.state.pathHovered &&
            <div key={'path_info'} style={{ backgroundColor: '#FFF', position: 'absolute', bottom: 0, width: '100%', height: 400 }}>
              <PathInfo path={this.state.selectedPath} />
            </div>
          }
        </Transition>
      </div>
    );
  }
}

export default App;
