import React, { Component } from 'react';
import styled from 'styled-components';

const capitalizeFirstLetter = (word) => {
    return word.charAt(0).toUpperCase() + word.slice(1);
}

const Button = styled.button`
  flex: 1;
  width: 100%;
  color: rgba(0,0,0,0.6);
  font-size: 16px;
  padding: 10px 15px;
  margin-bottom: 5px;
  background: #fff;
  border: none;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.1);
  transition: box-shadow 0.1s ease, width 0.1s ease;
  &:hover {
    width: 120%;
    cursor: pointer;
  }
  &:active {
    background: #f4f4f4;
  }
  &:focus {
    outline:0;
  }
`

class PathList extends Component {
  render() {
    return (
      <div style={{ display: 'flex', flexDirection: 'column', padding: '10px 10px' }}>
        {this.props.paths.map(path => (
          <div key={path.path_name.sv}>
            <Button onClick={() => this.props.onPathSelect(path)}>
              {capitalizeFirstLetter(path.path_name.sv)}
            </Button>
          </div>
        ))}
      </div>
    );
  }
};

export default PathList;