import React, { Component } from 'react';

const capitalizeFirstLetter = (word) => {
    return word.charAt(0).toUpperCase() + word.slice(1);
}

class PlaceInfo extends Component {
  render() {
    return (
      <div style={{ display: 'flex', flexDirection: 'row', height: '100%', color: 'black' }}>
        {this.props.place.place_image &&
          <div style={{ maxWidth: 400, padding: '0px 15px', display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
            <img src={this.props.place.place_image} style={{ width: '100%', boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)' }}></img>
          </div>
        }
        <div style={{ flex: 1, padding: '15px 15px', color: 'rgba(0,0,0,0.6)', fontSize: 46, display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          {capitalizeFirstLetter(this.props.place.place_name.sv)}
        </div>
      </div>
    );
  }
};

export default PlaceInfo;