import React, { Component } from 'react';

const capitalizeFirstLetter = (word) => {
    return word.charAt(0).toUpperCase() + word.slice(1);
}

class PathInfo extends Component {
  render() {
    return (
      <div style={{ display: 'flex', flexDirection: 'row', height: '100%', color: 'black' }}>
        {this.props.path.path_image &&
          <div style={{ width: 180, padding: '0px 15px', display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
            <img src={this.props.path.path_image} style={{ display: 'block', width: '100%', height: 'auto', boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)' }}></img>
          </div>
        }
        <div style={{ flex: 1, padding: '15px 15px', color: 'rgba(0,0,0,0.6)', display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
          <div style={{ fontSize: 46 }}>{capitalizeFirstLetter(this.props.path.path_name.sv)}</div>
          <div style={{ fontSize: 30 }}>{capitalizeFirstLetter(this.props.path.path_info.sv)}</div>
        </div>
      </div>
    );
  }
};

export default PathInfo;