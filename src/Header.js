import React from 'react';

const Header = props => (
  <h1>{props.label}</h1>
);

export default Header;